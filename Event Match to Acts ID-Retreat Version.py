#!/usr/bin/env python
# coding: utf-8

# # Retreat Attendance Matching to Acts 2:47 ID
# ## Process and future steps
# -------
# 1. Import CSV of event attendees into Python  
#     -Needs to have column titles of "First", "Last", "Email", "Phone"  
#     -Needs to have Phone with no special characters (could be done in Python in future)
#     -Should we drop any missing values?
# ***
# 2. Extract necessary data 
# ***
# 3. Match against each other using phone, email, etc  
# ***
# 4. Export into csv
# 
# 5. Additional to be done in future (that means you, Chris!)  
#      -Clean up phone values (reduce down to 9 or 10 raw integers, nothing else)
#      -Add in comments below to make sure process is clear and documented  
#      -Use second email address from db to add further layer of checking  
#      -brainstorm additional way to increase match %  
#
# 6. BP edits to be made: commenting, git pushes, remove acts csv import, no need
#      

# In[1]:


#Import needed libraries
import pandas as pd
from pandas import DataFrame
import os
import csv
import sshtunnel
import pymysql


# In[2]:



#title event (or grouping), to be used to name exported csv
#####
title_of_event = 'Retreats in FY18'
#####


event_students = pd.read_csv('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\Master Canon of Event Metrics\\FY18 Retreat Dataset.csv',encoding='latin1')

event_students["concat"] = event_students["First"] + event_students["Last"]

event_students = event_students[['Retreat', 'Start Date', 'State in Life','First', 'Last', 'concat','Email', 'Phone' ]]

##Future: look into if any dropping would have merits
#event_students = event_students.dropna(axis=0)

event_students = event_students.drop_duplicates()

len(event_students)

event_students = event_students.rename(columns = {'State in Life':'State_in_Life'}) 


event_students = event_students[event_students.State_in_Life == 'Student']

event_students.head()


# In[3]:


print(event_students.describe())

#to remove students that have neither phone or email
event_students = event_students.dropna(thresh=7)


len(event_students)


# In[4]:


event_students.head(20)


# ## Use one of the 2 below code blocks:  
# 1. The first one should be used if no connection to the Acts database is available as it instead reads in a csv of all the current IDS
# 2. The second one is preferred as it will pull the most recent ID list from the database

# In[5]:


#Code Block 1

acts_students = pd.read_csv('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\Data Cleaning\\FY19 total student ids.csv')


acts_students["concat"] = acts_students["first_name"] + acts_students["last_name"]

acts_students = acts_students[['id','first_name', 'last_name', 'concat','email', 'phone_number']]

acts_students.dropna(axis=0)

len(acts_students)


# In[6]:


#Code Block 2

ssh_key_path=r'id_rsa'

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, U.first_name, U.last_name, concat(U.first_name, U.last_name) as 'concat', \n
                        U.email, UP.phone_number \n
                    from users as U \n
                    left join user_phones as UP on UP.user_id = U.id \n
                    where U.user_role_type_id = 3  \n
                """
            args = []
            cursor.execute(sql, args)
            result = cursor.fetchall()
            current_acts_student_ids = len(result)
            all_acts_students = DataFrame(result)
            print('Current # of students in Acts db: ' , current_acts_student_ids)
    finally:
        connection.close()


# In[7]:


all_acts_students.head(20)


# In[23]:


all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace('-', '')
all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace(' ', '')
all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace('(', '')
all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace(')', '')
all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace('\'', '')
all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace('\"', '')
all_acts_students['phone_number']=all_acts_students['phone_number'].str.replace('  ', '')

all_acts_students.phone_number.fillna(value=0, inplace=True)

all_acts_students['phone_number'].str.strip()

all_acts_students['phone_number'] = pd.to_numeric(all_acts_students['phone_number'], errors='coerce')



# In[24]:


all_acts_students.dtypes


# In[9]:


#event_students["Phone"] = event_students["Phone"].astype(int)
event_students.dtypes

#event_students


# In[10]:


all_acts_students.dtypes

all_acts_students.phone_number = all_acts_students.phone_number.astype(float)


# In[11]:


all_acts_students_with_email = all_acts_students.dropna(subset=['email'])

all_acts_students_with_email

matched_df = event_students.merge(all_acts_students_with_email[['id', 'email']], 
                   left_on = 'Email', right_on = 'email', how = 'left')

matched_df = matched_df.drop_duplicates()

len(matched_df)


# In[12]:


print(len(matched_df), len(all_acts_students_with_email))


# ## Matching on phone
# 
# -Need to first remove rows with NULLs or they'll match on the event students with no phones

# In[13]:


all_acts_students_no_phone = all_acts_students.dropna(subset=['phone_number'])

all_acts_students_no_phone

#len(all_acts_students_no_phone)


# In[14]:


matched_df2 = matched_df.merge(all_acts_students_no_phone[['id','phone_number']],
                       left_on = 'Phone', right_on = 'phone_number', how = 'left')

matched_df2 = matched_df2.drop(['email','phone_number'], axis=1)

matched_df2 = matched_df2.rename(columns = {'id_x':'email_id', 'id_y':'phone_id'}) 

matched_df2.head(20)


# In[ ]:





# In[15]:


matched_df2 = matched_df2.drop_duplicates('Email')

matched_df2['best_id'] = matched_df2['email_id']

matched_df2.best_id.fillna(matched_df2.phone_id, inplace=True)

matched_df2["Concat"] = matched_df2["First"] + matched_df2["Last"]


print(len(matched_df2))

matched_df2.head(100)


# In[16]:


#Check to see how many didn't receive any id

#print(len(matched_df2[['email_id','phone_id']][matched_df2['email_id'].isnull()][matched_df2['phone_id'].isnull()]))


# In[17]:


all_acts_students["concat"] = all_acts_students["first_name"] + all_acts_students["last_name"]

all_acts_concat = all_acts_students.drop_duplicates(subset=['concat'])



matched_df3 = matched_df2.merge(all_acts_concat[['id','concat']],
                       left_on = 'Concat', right_on = 'concat', how = 'left')

matched_df3 = matched_df3.rename(columns = {'id':'concat_id'}) 

matched_df3 = matched_df3.drop(columns=['concat_x', 'concat_y'])

matched_df3 = matched_df3[['Retreat', 'Start Date', 'State_in_Life', 'First', 'Last', 'Email', 'Phone','Concat', 'email_id', 'phone_id', 'concat_id', 'best_id']]

matched_df3.best_id.fillna(matched_df3.concat_id, inplace=True)

print(len(matched_df3))

matched_df3.head(100)


# In[18]:


email_ids = matched_df3['email_id'].count()

phone_ids = matched_df3['phone_id'].count()

concat_ids = matched_df3['concat_id'].count()

best_ids = matched_df3['best_id'].count()

total_event_stu = len(matched_df3)

print("Percent Matched Using Emails:", round(100*(email_ids/total_event_stu),2))

print("Percent Matched Using Phone Numbers:", round(100*(phone_ids/total_event_stu),2))

print("Percent Matched Using Concatenated Names:", round(100*(concat_ids/total_event_stu),2))

print("Percent Matched Using All Three Methods (preference on email match):", round(100*(best_ids/total_event_stu),2))


# In[21]:


final_df = matched_df3[['First', 'Last', 'Retreat', 'Start Date', 'best_id']]

final_df = final_df.dropna(subset=['best_id'])

print('Final lenth of ID csv: ',len(final_df))

if os.path.exists('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\Data Cleaning'):
    final_df.to_csv('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\Data Cleaning\\'+title_of_event+'_ids_matched.csv')
    print("Success!  Final list of conference student IDs can be found in the Data Cleaning Folder")
else: 
    print("No go!  Something went awry, go back and find the bugger")

